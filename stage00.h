#ifndef STAGE00_H__
#define STAGE00_H__

void draw_cube(Dynamic* dynamicp);
void draw_room(Dynamic* dynamicp);

static float degreesX = 0.0F;
static float degreesY = 0.0F;
static float degreesZ = 0.0F;
static u16 perspNorm = 0;

extern NUContData ContTrigger[NU_CONT_MAXCONTROLLERS];

/* 1 diffuse light */
static Lights2 my_light = gdSPDefLights2(
	10, 10, 10, /* ambient light */
	0, 200, 0, -80, 80, 0, 	/* colored light color and origin */
	200, 0, 0, 80, 80, 0); /* colored light color and origin */

Mtx mCubeTranslate;
Mtx mCubeRotate;
Mtx mCubeScale;
Mtx mCubeTransformation;

Mtx mRoomTranslate;
Mtx mRoomRotate;
Mtx mRoomScale;
Mtx mRoomTransformation;

Mtx mView;
Mtx mProjection;

#endif
