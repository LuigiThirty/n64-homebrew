/*
   stage00.c 

   Copyright (C) 1997-1999, NINTENDO Co,Ltd.
*/

#include <assert.h>
#include <nusys.h>
#include "graphic.h"
#include "font.h"
#include "stage00.h"
#include "texture_c/hex_16x16.h"

static int textline_x = 10;
static int textline_y = 10;
static int textindex = 0;

/* Make the display list and activate the task. */
void makeDL00(void)
{	
	/* Specify the display list buffer */
	p_glist = gfx_glist;

	/* Routine for initializing the RCP in graphic.c */
	gfxRCPInit();

	/* Clear the frame buffer and the Z-buffer */
	gfxClearCfb();

	guLookAt(&mView,
		cameraPosition.x, cameraPosition.y, cameraPosition.z,
		0, 0, 0,
		0, 1, 0);

	/* Set up a perspective projection matrix. */
	guPerspective(&mProjection,
		&perspNorm,
		33.0F,
		(float)(320.0/240.0),
		1.0F,
		100.0F,
		1.0F);
	guMtxCatL(&mView, &mProjection, &gfx_dynamic.projection);

	/* Push the projection matrix onto the projection matrix stack. */
	gSPMatrix(p_glist++, OS_K0_TO_PHYSICAL(&gfx_dynamic.projection), 
		G_MTX_PROJECTION|G_MTX_LOAD|G_MTX_NOPUSH);

	/* Draw a square. */
	draw_cube(&gfx_dynamic);
	draw_room(&gfx_dynamic);

	strcpy(outstring, "This is Kate 64.\0");

	/* From font.c, sets up for rendering decals */
    gDPSetCycleType(p_glist++, G_CYC_1CYCLE);
    gDPSetTextureFilter(p_glist++, G_TF_POINT);
    gDPSetRenderMode(p_glist++, G_RM_TEX_EDGE, G_RM_TEX_EDGE);

    /* Set up for texturing... */
	gSPTexture(p_glist++, 0xFFFF, 0xFFFF, 0, G_TX_RENDERTILE, G_ON);
    gDPSetCombineMode(p_glist++, G_CC_DECALRGBA, G_CC_DECALRGBA);
    gDPSetTexturePersp(p_glist++, G_TP_NONE);

    /* Load the hex font texture */
	gDPLoadTextureBlock_4b(p_glist++,
		hex_16x16_tmf_img,		/* 64-bit aligned texture array */
		G_IM_FMT_CI,				/* image format (from texture info) */
		32, 32,					/* texture dimensions (1-4096) */
		0,						/* palette address */
		G_TX_WRAP, G_TX_WRAP,		/* S axis flags */
		G_TX_NOMASK, G_TX_NOMASK,	/* T axis flags */
		G_TX_NOLOD, G_TX_NOLOD);	/* (S,T) coordinate shift? */

	textline_x = 16;

	for(textindex = 0; textindex < 16; textindex++)
	{
		/* Put the texture on the screen */
		gSPTextureRectangle(p_glist++,
			(int)( textline_x ) << 2, 			/* X */
			(int)( textline_y ) << 2, 			/* Y */
			(int)( textline_x + 8 ) << 2, 		/* rightX */
			(int)( textline_y + 8 ) << 2, 		/* rightY */
			G_TX_RENDERTILE,			/* tile descriptor method */
			(((textindex % 4) * 8) << 5),					/* S */
			(((textindex / 4) * 8) << 5),					/* T */
			(int)(1 << 10),				/* delta S per X */
			(int)(1 << 10)				/* delta T per Y */
			);

		textline_x += 8;
	}

	/* End the construction of the display list  */

	/* End of frame. */
	gDPFullSync(p_glist++);
	/* End of display list. */
	gSPEndDisplayList(p_glist++);

	/* Check if all are put in the array  */
	assert(p_glist - gfx_glist < GFX_GLIST_LEN);

	/* Activate the RSP task.  Switch display buffers at the end of the task. */
	nuGfxTaskStart(gfx_glist,
		 (s32)(p_glist - gfx_glist) * sizeof (Gfx),
		 NU_GFX_UCODE_F3DEX , NU_SC_SWAPBUFFER);
}

#define VERTEX_DEF(X, Y, Z, U, V, R, G, B, A) { X, Y, Z, 0, U, V, R, G, B, A }

/* Vertex coordinates.  */
static Vtx shade_vtx[] =  {
	VERTEX_DEF(-1, -1, 1, 		0, 0, 	0x00, 0x00, 0xC0, 0xFF),
	VERTEX_DEF(-1, -1, 1, 		0, 0, 	0x00, 0x00, 0xC0, 0xFF),
	VERTEX_DEF(1, -1, 1, 		0, 0, 	0x00, 0xC0, 0x00, 0xFF),
	VERTEX_DEF(1, -1, -1, 		0, 0, 	0x00, 0xC0, 0xC0, 0xFF),
	VERTEX_DEF(-1, -1, -1, 		0, 0, 	0xC0, 0x00, 0x00, 0xFF),
	VERTEX_DEF(-1, 1, 1, 		0, 0, 	0xC0, 0x00, 0xC0, 0xFF),
	VERTEX_DEF(1, 1, 1, 		0, 0, 	0xC0, 0xC0, 0x00, 0xFF),
	VERTEX_DEF(1, 1, -1, 		0, 0, 	0xC0, 0xC0, 0xC0, 0xFF),
	VERTEX_DEF(-1, 1, -1, 		0, 0, 	0x80, 0x80, 0x80, 0xFF),
};

static Vtx room_vtx[] = {
	VERTEX_DEF( -5, -1, 2, 0, 0, 0xC0, 0xC0, 0xC0, 0xFF ),
	VERTEX_DEF( 5, -1, 2, 0, 0, 0xC0, 0xC0, 0xC0, 0xFF ),
	VERTEX_DEF( 5, -1, -2, 0, 0, 0xC0, 0xC0, 0xC0, 0xFF ),
	VERTEX_DEF( -5, -1, -2, 0, 0, 0xC0, 0xC0, 0xC0, 0xFF ),
	VERTEX_DEF( -5, 1, 2, 0, 0, 0xC0, 0xC0, 0xC0, 0xFF ),
	VERTEX_DEF( 5, 1, 2, 0, 0, 0xC0, 0xC0, 0xC0, 0xFF ),
	VERTEX_DEF( 5, 1, -2, 0, 0, 0xC0, 0xC0, 0xC0, 0xFF ),
	VERTEX_DEF( -5, 1, -2, 0, 0, 0xC0, 0xC0, 0xC0, 0xFF ),
};

void draw_room(Dynamic * dynamicp)
{
	/* Set the model matrix's transform. */

	guTranslate(&mRoomTranslate, 0.0F, -5.0F, 0.0F);
	guRotateRPY(&mRoomRotate, 0.0F, 0.0F, 0.0F);
	guScale(&mRoomScale, 1.0F, 1.0F, 1.0F);
	guMtxCatL(&mRoomRotate, &mRoomTranslate, &mRoomTransformation);

	/* Push the ModelView matrix onto the ModelView matrix stack. */
	gSPMatrix(p_glist++, OS_K0_TO_PHYSICAL(&mRoomTransformation),
		G_MTX_MODELVIEW|G_MTX_LOAD|G_MTX_NOPUSH);

	/* Sets the RDP's rendering cycle. See chapter 12 of the N16 manual. */
	gDPSetCycleType(p_glist++, G_CYC_1CYCLE);

	/* Sets the rendering mode of the BL (blender). Takes two arguments - mode1 and mode2. */
	/* This sets it for anti-aliased opaque surfaces. */
	gDPSetRenderMode(p_glist++, G_RM_AA_OPA_SURF, G_RM_AA_OPA_SURF2);

	/* Resets the geometry pipeline modes, disabling all of them. */
	gSPClearGeometryMode(p_glist++, 0xFFFFFFFF);

	/* Enables G_SHADE (vertex color) and G_SHADING_SMOOTH (Gouraud shading) geometry modes. */
	gSPSetGeometryMode(p_glist++, G_SHADE | G_SHADING_SMOOTH);

	/* Draw triangles with the specified vertex indices from the vertex cache. */
	/* 32 triangles per cache before reloading it. */
	/* Load vertices into the vertex cache. */
	gSPVertex(p_glist++, &(room_vtx[0]), 8, 0);

	gSP1Triangle(p_glist++, 3, 1, 0, 0);
	gSP1Triangle(p_glist++, 3, 2, 1, 0);
	gSP1Triangle(p_glist++, 4, 0, 1, 0);
	gSP1Triangle(p_glist++, 5, 4, 1, 0);
	gSP1Triangle(p_glist++, 2, 5, 1, 0);
	gSP1Triangle(p_glist++, 6, 5, 2, 0);
	gSP1Triangle(p_glist++, 7, 2, 3, 0);
	gSP1Triangle(p_glist++, 7, 6, 2, 0);
	gSP1Triangle(p_glist++, 4, 3, 0, 0);
	gSP1Triangle(p_glist++, 4, 7, 3, 0);
	gSP1Triangle(p_glist++, 7, 4, 5, 0);
	gSP1Triangle(p_glist++, 6, 7, 5, 0);

	/* RDP attribute sync. Always goes after primitives. */
	gDPPipeSync(p_glist++);
}

/* Draw a square. */
void draw_cube(Dynamic * dynamicp)
{
	/* Set the model matrix's transform. */
	guTranslate(&mCubeTranslate, 0.0F, 0.0F, 0.0F);
	guRotateRPY(&mCubeRotate, degreesX, degreesY, degreesZ);
	guScale(&mCubeScale, 1.0F, 1.0F, 1.0F);
	guMtxCatL(&mCubeRotate, &mCubeTranslate, &mCubeTransformation);
	gSPMatrix(p_glist++, OS_K0_TO_PHYSICAL(&mCubeTransformation),
		G_MTX_MODELVIEW|G_MTX_LOAD|G_MTX_NOPUSH);

	/* 2 dynamic lights. */
	//gSPSetLights2(p_glist++, my_light);

	/* RDP attribute sync. Always goes after primitives. */
	gDPPipeSync(p_glist++);

	/* Sets the RDP's rendering cycle. See chapter 12 of the N16 manual. */
	gDPSetCycleType(p_glist++, G_CYC_1CYCLE);

	/* Sets the rendering mode of the BL (blender). Takes two arguments - mode1 and mode2. */
	/* This sets it for anti-aliased opaque surfaces. */
	gDPSetRenderMode(p_glist++, G_RM_ZB_OPA_SURF, G_RM_ZB_OPA_SURF2);

	/* Resets the geometry pipeline modes, disabling all of them. */
	gSPClearGeometryMode(p_glist++, 0xFFFFFFFF);

	/* Enables G_SHADE (vertex color) and G_SHADING_SMOOTH (Gouraud shading) geometry modes. */
	gSPSetGeometryMode(p_glist++, G_SHADE | G_SHADING_SMOOTH | G_CULL_BACK);

	/* Draw triangles with the specified vertex indices from the vertex cache. */
	/* 32 triangles per cache before reloading it. */
	/* Load vertices into the vertex cache. */
	gSPVertex(p_glist++, &(shade_vtx[0]), 9, 0);

	/* Define triangles to draw. */
	gSP1Triangle(p_glist++, 4,2,1, 0);
	gSP1Triangle(p_glist++, 4,3,2, 0);
	gSP1Triangle(p_glist++, 5,1,2, 0);
	gSP1Triangle(p_glist++, 6,5,2, 0);

	gSP1Triangle(p_glist++, 3,6,2, 0);
	gSP1Triangle(p_glist++, 7,6,3, 0);
	gSP1Triangle(p_glist++, 8,3,4, 0);
	gSP1Triangle(p_glist++, 8,7,3, 0);

	gSP1Triangle(p_glist++, 5,4,1, 0);
	gSP1Triangle(p_glist++, 5,8,4, 0);
	gSP1Triangle(p_glist++, 8,5,6, 0);
	gSP1Triangle(p_glist++, 7,8,6, 0);

	/* RDP attribute sync. Always goes after primitives. */
	gDPPipeSync(p_glist++);
}

void Stage00Loop()
{
	// Read the controller.
	/*
	if(ContTrigger[0].button & A_BUTTON)
	{
		degrees++;
	}
	*/

	/*
	degreesX += ContTrigger[0].stick_y * 0.025;
	degreesY += ContTrigger[0].stick_x * 0.025;
	*/

	if(ContTrigger[0].button & A_BUTTON)
	{
		cameraPosition.z += 1;		
	}
	if(ContTrigger[0].button & B_BUTTON)
	{
		cameraPosition.z -= 1;		
	}

	degreesX++;
	degreesY++;
	degreesZ++;

}