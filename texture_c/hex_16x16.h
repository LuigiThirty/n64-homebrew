#ifndef __HEX_16X16_H__
#define __HEX_16X16_H__

#ifndef	_teTMF_
#define	_teTMF_

struct teTMF {
	u32*	img;
	u32		fmt;
	u32		siz;
	u32		width;
	u32		height;
};

#endif

#ifndef	_teTLF_
#define	_teTLF_

struct teTLF {
	u32		type;
	u16*	tlut;
	u16		size;
};

#endif

#ifndef	_tePicture_
#define	_tePicture_

struct tePicture {
	struct teTMF*	tmf;
	struct teTLF*	tlf;
	u32	pal;
};

#endif
#ifndef	elements
#define	elements(a)	(sizeof (a) / sizeof *(a))
#endif
extern struct teTMF hex_16x16_tmf;
extern u16 hex_16x16_tlf_tlut[];
extern struct teTLF hex_16x16_tlf;
extern struct tePicture hex_16x16;
extern u32 hex_16x16_tmf_img[];

#endif /* __HEX_16X16_H__ */
