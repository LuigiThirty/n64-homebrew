#include "main.h"
#include "graphic.h"

NUContData ContTrigger[NU_CONT_MAXCONTROLLERS];
Vector3f cameraPosition;

static u16* FrameBuf3[3] = {
	(u16*)NU_GFX_FRAMEBUFFER0_ADDR,
	(u16*)NU_GFX_FRAMEBUFFER1_ADDR,
	(u16*)NU_GFX_FRAMEBUFFER2_ADDR,
};

/*-----------------------------------------------------------------------------
  The call-back function 

  pendingGfx which is passed from Nusystem as the argument of the call-back 
  function is the total number of RCP tasks that are currently processing 
  and waiting for the process. 
-----------------------------------------------------------------------------*/
void stage00(int pendingGfx)
{
	nuGfxSetCfb(FrameBuf3, 3);

  /* It provides the display process if there is no RCP task that is processing. */
  	if(pendingGfx < 2)
    	makeDL00();		

	nuContDataGetExAll(&ContTrigger[0]);
	Stage00Loop();
}

void mainproc(void)
{
	cameraPosition.x = 0;
	cameraPosition.y = 5;
	cameraPosition.z = 10;

	/* Initialize graphics and the controller manager */
	nuGfxInit();
	nuContInit();
	
	/* Register the stage00 callback  */
	nuGfxFuncSet((NUGfxFunc)stage00);
	
	/* Screen on! */
	nuGfxDisplayOn();
	
	while(1)
	{
	};
}