#include <nusys.h>
#include "graphic.h"

Gfx gfx_glist[GFX_GLIST_LEN];
Dynamic gfx_dynamic;
Gfx *p_glist;

/* gfxRCPInit - initialize RCP */
void gfxRCPInit()
{
	/* Set the RSP segment register */
	gSPSegment(p_glist++, 0, 0x0); /* Virtual address */
	
	/* Set up the RSP */
	gSPDisplayList(p_glist++, OS_K0_TO_PHYSICAL(setup_rspstate));
	
	/* Set up the RDP */
	gSPDisplayList(p_glist++, OS_K0_TO_PHYSICAL(setup_rdpstate));
}

/* 	
	gfxClearCfb
	Set address of the framebuffer and Z buffer, then erase them
	using nuGfxZBuffer and nuGfxCfb_ptr - global variables 
*/
void gfxClearCfb(void)
{
	/* Clear the Z-buffer */
	gDPSetDepthImage(p_glist++, OS_K0_TO_PHYSICAL(nuGfxZBuffer));
	gDPSetCycleType(p_glist++, G_CYC_FILL);
	gDPSetColorImage(p_glist++, G_IM_FMT_RGBA, G_IM_SIZ_16b,SCREEN_WD, OS_K0_TO_PHYSICAL(nuGfxZBuffer));
	gDPSetFillColor(p_glist++,(GPACK_ZDZ(G_MAXFBZ,0) << 16 | GPACK_ZDZ(G_MAXFBZ,0)));
	
	gDPFillRectangle(p_glist++, 0, 0, SCREEN_WD-1, SCREEN_HT-1);
	gDPPipeSync(p_glist++);
	
	/* Clear the frame buffer  */
	gDPSetColorImage(p_glist++, G_IM_FMT_RGBA, G_IM_SIZ_16b, SCREEN_WD, osVirtualToPhysical(nuGfxCfb_ptr));
	gDPSetFillColor(p_glist++, (GPACK_RGBA5551(0, 0, 0, 1) << 16 | GPACK_RGBA5551(0, 0, 0, 1)));
	
	/* Draw something */
	gDPFillRectangle(p_glist++, 0, 0, SCREEN_WD-1, SCREEN_HT-1);
	
	gDPPipeSync(p_glist++);
}